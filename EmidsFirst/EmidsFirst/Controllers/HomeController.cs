﻿using EmidsFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EmidsFirst.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpGet]
        public async Task<ActionResult> SearchSuggestions(string serachText)
        {
            var obj = await new SearchSuggestionsAPI().GetEmidsSharepointAPI(serachText);
            return View("ResultPage", obj);

        }


        [HttpGet]
        public async Task<JsonResult> AutoComplete(string serachText)
        {
            var obj = await new SearchSuggestionsAPI().AutoComplete(serachText);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

    }
}