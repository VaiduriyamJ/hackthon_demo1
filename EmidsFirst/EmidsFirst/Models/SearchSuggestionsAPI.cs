﻿using DuckDuckGo.Net;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Search.Query;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Threading.Tasks;


namespace EmidsFirst.Models
{
    public class SearchSuggestionsAPI
    {
        /// <summary>
        /// The Google Suggest search URL.
        /// </summary>
        /// <remarks>
        /// Add gl=dk for Google Denmark. Add lr=lang_da for danish results. Add hl=da to indicate the language of the UI making the request.
        /// </remarks>
        private const string _suggestSearchUrl = "http://www.google.com/complete/search?output=toolbar&q={0}&hl=en";

        /// <summary>
        /// Gets the search suggestions from Google.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A list of <see cref="GoogleSuggestion"/>s.</returns>
        public async Task<List<GoogleSuggestion>> GetAutoSuggestions(string query)
        {
            List<GoogleSuggestion> objtest = new List<GoogleSuggestion>();

            if (String.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentException("Argument cannot be null or empty!", "query");
            }

            try
            {
                using (HttpClient client = new HttpClient())
                {

                    var result = await client.GetStringAsync(string.Format(_suggestSearchUrl, query));

                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return objtest;
        }


        public List<EmidsSuggestion> GetEmidsSearchSuggestions(string serachText)
        {
            List<EmidsSuggestion> objtest = new List<EmidsSuggestion>();
            try
            {
                SecureString theSecureString = new NetworkCredential("", "Change123").SecurePassword;
                using (ClientContext clientContext = new ClientContext("https://emids.sharepoint.com/"))
                {
                    clientContext.Credentials = new SharePointOnlineCredentials("Sharepoint-int@emids.com", theSecureString);
                    KeywordQuery keywordQuery = new KeywordQuery(clientContext);
                    keywordQuery.QueryText = "help";
                    SearchExecutor searchExecutor = new SearchExecutor(clientContext);
                    ClientResult<ResultTableCollection> results = searchExecutor.ExecuteQuery(keywordQuery);
                    clientContext.ExecuteQuery();

                    Console.WriteLine(" *** Query Search with SharePoint 2013 Client Object Model *** \n");
                    Console.WriteLine(" Below are the search results for your Query Text = " + keywordQuery.QueryText + "\n");

                    foreach (var resultRow in results.Value[0].ResultRows)
                    {
                        Console.WriteLine("Title: {0} Path: {1} Last Modified At: {2}",
                            resultRow["Title"] + "\n", resultRow["Path"] + "\n", resultRow["LastModifiedTime"]);
                        Console.WriteLine("\n");
                        Console.ReadLine();
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return objtest;

        }

        public async Task<List<EmidsSuggestion>> GetEmidsSharepointAPI(string serachText)
        {
            List<EmidsSuggestion> getSuggestion = new List<EmidsSuggestion>();
            RootObject getJson = new RootObject();
            string str = "\'" + serachText + "\'";
            try
            {
                var webUri = new Uri("https://emids.sharepoint.com/");
                const string userName = "Sharepoint-int@emids.com";
                const string password = "Change123";
                var securePassword = new SecureString();
                foreach (var c in password)
                {
                    securePassword.AppendChar(c);
                }
                var credentials = new SharePointOnlineCredentials(userName, securePassword);

                using (var client = new WebClient())
                {
                    client.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                    client.Credentials = credentials;
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/json;odata=verbose");
                    client.Headers.Add(HttpRequestHeader.Accept, "application/json;odata=verbose");
                    //var test = string.Format("_api/search/query?querytext=" + str + "&amp;startrow=10");
                    var endpointUri = new Uri(webUri, string.Format("_api/search/query?querytext=" + str + "&amp;startrow=10"));
                    var result = client.DownloadString(endpointUri);
                    getJson = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(result);
                    if (getJson.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results.Count > 0)
                    {
                        foreach (var itemGroup in getJson.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results)
                        {
                            var subItem = new EmidsSuggestion();
                            foreach (var item in itemGroup.Cells.results)
                            {
                                if (item.Key == "Title")
                                {
                                    subItem.Title = item.Value != null ? item.Value.ToString() : String.Empty;
                                }

                                if (item.Key == "Path")
                                {
                                    subItem.Path = item.Value != null ? item.Value.ToString() : String.Empty;
                                }

                                if (item.Key == "LastModifiedTime")
                                {
                                    subItem.LastModifiedTime = item.Value != null ? item.Value.ToString() : String.Empty;
                                }

                                if (item.Key == "HitHighlightedSummary")
                                {
                                    subItem.HitHighlightedSummary = item.Value != null ? item.Value.ToString() : String.Empty;
                                }

                            }
                            getSuggestion.Add(subItem);
                        }

                    }

                    if (getSuggestion.Count < 10)
                    {
                        var GetBrowserResult = GetBrowserResults(str).GetRange(0, 10 - (getSuggestion.Count == null ? 0 : getSuggestion.Count));
                        foreach (var item in GetBrowserResult)
                        {
                            var subItem = new EmidsSuggestion();
                            subItem.Title = item.Text;
                            subItem.Path = item.FirstURL;
                            subItem.HitHighlightedSummary = item.Result;
                            getSuggestion.Add(subItem);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                getSuggestion = null;
            }
            return getSuggestion;
        }

        internal List<BrowserResults> GetBrowserResults(string serachText)
        {
            List<BrowserResults> getData = new List<BrowserResults>();
            try
            {
                var search = new Search
                {
                    NoHtml = true,
                    NoRedirects = true,
                    IsSecure = true,
                    SkipDisambiguation = true,
                    ApiClient = new HttpWebApi()
                };
                var jsonString = search.TextQuery(serachText, "Testing", ResponseFormat.Json);
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Sample>(jsonString);
                getData = result.RelatedTopics;

            }
            catch (Exception ex)
            {
                getData = null;
            }
            return getData;
        }

        public async Task<List<AutocompleteModel>> AutoComplete(string serachText)
        {
            List<AutocompleteModel> pullSuggestion = new List<AutocompleteModel>();
            try
            {
                string Uri = "https://duckduckgo.com/ac/?q=" + serachText;
                System.Net.WebRequest request = System.Net.WebRequest.Create(Uri);
                System.Net.WebResponse response = request.GetResponse();
                System.IO.StreamReader strReader = new System.IO.StreamReader(response.GetResponseStream());
                string result = strReader.ReadToEnd();
                pullSuggestion = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AutocompleteModel>>(result);
                

            }
            catch (Exception ex)
            {
            }
            return pullSuggestion;

        }
    }

    public class AutocompleteModel
    {
        public string phrase { get; set; }
    }

    /// <summary>
    /// Encapsulates a suggestion from Google.
    /// </summary>
    public class GoogleSuggestion
    {
        /// <summary>
        /// Gets or sets the phrase.
        /// </summary>
        /// <value>The phrase.</value>
        public string Phrase { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Phrase;
        }
    }


    /// <summary>
    /// Encapsulates a suggestion from Google.
    /// </summary>
    public class EmidsSuggestion
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public string LastModifiedTime { get; set; }
        public string HitHighlightedSummary { get; set; }


    }
    public class BrowserResults
    {
        public string Text { get; set; }
        public string FirstURL { get; set; }
        public string Result { get; set; }

    }

    public class Sample
    {
        public List<BrowserResults> RelatedTopics { get; set; }
    }


}